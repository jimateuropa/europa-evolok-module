function update_ev_sub_date(event) {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	} 
	if (mm < 10) {
		mm = '0' + mm;
	} 
	var todayDate = yyyy + "-" + mm + '-' + dd;
	var dateAttrs = [{"name": Drupal.settings.europa.subDateAttribute, "value": todayDate}];
	EV.Core.updateUserProfile(event.response.userProfile.guid, Drupal.settings.europa.subDateService, dateAttrs);
}

EV.Event.on(EV.Event.RESET_PASSWORD_SUCCESS, function(res) {
	window.location.replace(window.location.protocol+"//"+window.location.hostname+"/subscription");
});

EV.Event.on(EV.Event.FORGOT_PASSWORD_SUCCESS, function(res) {
   setTimeout(function(){
	   window.scrollBy(0,150);
	   document.querySelector("button[ng-if=\"closeBtnCaption\"]").innerHTML = "Back to login";
   });
});

EV.Event.on("login.success", function (res) {
    //IF USER IS LOGGED IN BUT DOESN'T HAVE THE OPT IN TICKED
    if (window.location.pathname == "/register" || window.location.pathname == "/evolok/register") {
        window.location.replace(window.location.protocol+"//"+window.location.hostname+"/subscription");
    }
    //reload the page
    else {
		// Get saved data from sessionStorage
		var data = sessionStorage.getItem("blockLoginReload");
		if (data != "true") { 	
			window.location.reload(true);
		}
    }
});

function countryIsFree(iso2) {
	function isMatch(element) {
		return element == iso2;
	}
	return [
	 	"AD",
	 	"AT",
	 	"BA",
	 	"BE",
	 	"CH",
	 	"CZ",
	 	"DE",
	 	"DK",
	 	"EE",
	 	"ES",
	 	"FI",
	 	"FR",
	 	"GB",
	 	"GI",
	 	"GR",
	 	"HR",
	 	"HU",
	 	"IE",
	 	"IL",
	 	"IM",
	 	"IS",
	 	"IT",
	 	"LI",
	 	"LT",
	 	"LU",
	 	"LV",
	 	"MC",
	 	"MD",
	 	"ME",
	 	"MK",
	 	"NL",
	 	"PL",
	 	"PT",
	 	"RO",
	 	"RS",
	 	"SE",
	 	"SI",
	 	"SK",
	 	"SM",
	 	"VA"
	 ].find(isMatch);
}

EV.Event.on(EV.Event.EDIT_PROFILE_COMPLETE_SUCCESS, function(event) {
	switch (event.widget_guid) {
		case Drupal.settings.europa.chooseReqOptinService:
			window.location.reload(true);
			break;
		case "evolok-myaccount":
			update_ev_sub_date(event);
			var session = EV.util.getEVSession();
			var service = "MyAccount";
			var country = false;
			//var attribute = "country";
			EV.Core.getUserProfile(session.guid, service, true).then(function(res) {
				country = res.response.userProfile.attributes.find(function(attr) {
					return attr.name == "country";//returns an obj
				});
				if (!countryIsFree(country.value)) {
					document.querySelector('#evolok-choose-products label[for="'+Drupal.settings.europa.printAttributeChooseProducts+'"]').parentElement.parentElement.style.display = "none";
				}
			});
			showModalIfNoOptin(EV.util.getEVSession());
			var el = document.getElementById("evolok-myaccount");
			el.className += " hide-widget-page";
			document.getElementById("evolok-myaccount").className = document.getElementById("evolok-myaccount").className.replace(new RegExp('(?:^|\\s)'+ 'show-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_next = document.getElementById("evolok-choose-products");
			el_next.className += " show-widget-page";
			document.getElementById("evolok-choose-products").className = document.getElementById("evolok-choose-products").className.replace(new RegExp('(?:^|\\s)'+ 'hide-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_progress = document.getElementById("0of3progress");
			el_progress.className += " hide-progress-widget";
			document.getElementById("0of3progress").className = document.getElementById("0of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'show-progress-widget' + '(?:\\s|$)'), ' ');
			
			var el_progress_next = document.getElementById("1of3progress");
			el_progress_next.className += " show-progress-widget";
			document.getElementById("1of3progress").className = document.getElementById("1of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'hide-progress-widget' + '(?:\\s|$)'), ' ');
			window.scroll(0,findPos(document.getElementById("progress-sub-widget-display"))-100);
			addDescriptionsToLabels("#evolok-choose-products");
			break;
		case "evolok-choose-products":
			update_ev_sub_date(event);
			/*var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1; //January is 0!
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd;
			} 
			if (mm < 10) {
				mm = '0' + mm;
			} 
			var todayDate = yyyy + "-" + mm + '-' + dd;
			var dateAttrs = [{"name": Drupal.settings.europa.subDateAttribute, "value": todayDate}];
			EV.Core.updateUserProfile(event.response.userProfile.guid, Drupal.settings.europa.subDateService, dateAttrs);*/
			var attrs = event.response.userProfile.attributes;
			var i;
			for (i = 0; i < attrs.length; i++) {
				if (attrs[i].name == Drupal.settings.europa.printAttributeChooseProducts && attrs[i].value == "true") {
					EV.Event.publish("ev.open.modal.evolok-address-confirmation");
					break;
				}
			}
			showModalIfNoOptin(EV.util.getEVSession());
			var el = document.getElementById("evolok-choose-products");
			el.className += " hide-widget-page";
			document.getElementById("evolok-choose-products").className = document.getElementById("evolok-choose-products").className.replace(new RegExp('(?:^|\\s)'+ 'show-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_next = document.getElementById("evolok-demogs");
			el_next.className += " show-widget-page";
			document.getElementById("evolok-demogs").className = document.getElementById("evolok-demogs").className.replace(new RegExp('(?:^|\\s)'+ 'hide-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_progress = document.getElementById("1of3progress");
			el_progress.className += " hide-progress-widget";
			document.getElementById("1of3progress").className = document.getElementById("1of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'show-progress-widget' + '(?:\\s|$)'), ' ');
			
			var el_progress_next = document.getElementById("2of3progress");
			el_progress_next.className += " show-progress-widget";
			document.getElementById("2of3progress").className = document.getElementById("2of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'hide-progress-widget' + '(?:\\s|$)'), ' ');
			window.scroll(0,findPos(document.getElementById("progress-sub-widget-display"))-100);
			addDescriptionsToLabels("#evolok-demogs");
			break;
		case "evolok-demogs":
			update_ev_sub_date(event);
			showModalIfNoOptin(EV.util.getEVSession());
			var el = document.getElementById("evolok-demogs");
			el.className += " hide-widget-page";
			document.getElementById("evolok-demogs").className = document.getElementById("evolok-demogs").className.replace(new RegExp('(?:^|\\s)'+ 'show-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_next = document.getElementById("complete-sub-widget");
			el_next.className += " show-widget-page";
			document.getElementById("complete-sub-widget").className = document.getElementById("complete-sub-widget").className.replace(new RegExp('(?:^|\\s)'+ 'hide-widget-page' + '(?:\\s|$)'), ' ');
			
			var el_progress = document.getElementById("2of3progress");
			el_progress.className += " hide-progress-widget";
			document.getElementById("2of3progress").className = document.getElementById("2of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'show-progress-widget' + '(?:\\s|$)'), ' ');
			
			var el_progress_next = document.getElementById("3of3progress");
			el_progress_next.className += " show-progress-widget";
			document.getElementById("3of3progress").className = document.getElementById("3of3progress").className.replace(new RegExp('(?:^|\\s)'+ 'hide-progress-widget' + '(?:\\s|$)'), ' ');
			window.scroll(0,findPos(document.getElementById("progress-sub-widget-display"))-100);
			break;
	}
	//Finds y value of given object (used above)
	function findPos(obj) {
	    var curtop = 0;
	    if (obj.offsetParent) {
	        do {
	            curtop += obj.offsetTop;
	        } while (obj = obj.offsetParent);
	    return [curtop];
	    }
	}
});


//widget field types

EV.Widgets.Display = {
		"do_not_contact": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_tech_focus": {"type": "checkbox", "values": [{"value": true}]},
		"electrooptics_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"fibresystems_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_tech_focus": {"type": "checkbox", "values": [{"value": true}]},
		"imagingandmachinevision_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"lasersystemseurope_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_event_preview": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"researchinformation_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_advertising": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_digital": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_editorial": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_hpc_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_informatics": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_mod_sim_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_newsline": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_print": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_productline": {"type": "checkbox", "values": [{"value": true}]},
		"scientificcomputingworld_subscription_wc_wp": {"type": "checkbox", "values": [{"value": true}]},
		"eurosci_optin": {"type": "checkbox", "values": [{"value": true}]},
		"eo_optin": {"type": "checkbox", "values": [{"value": true}]},
		"eo_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"fs_optin": {"type": "checkbox", "values": [{"value": true}]},
		"fs_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"imve_optin": {"type": "checkbox", "values": [{"value": true}]},
		"imve_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"lse_optin": {"type": "checkbox", "values": [{"value": true}]},
		"lse_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"ri_optin": {"type": "checkbox", "values": [{"value": true}]},
		"ri_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"scw_optin": {"type": "checkbox", "values": [{"value": true}]},
		"scw_third_party": {"type": "checkbox", "values": [{"value": true}]},
		"third_party_advertising_flag": {"type": "checkbox", "values": [{"value": true}]},
	    "country": {
	    	"type": "dropdown",
				"values": [
				{ "caption": "Afghanistan", "value": "AF" },
				{ "caption": "Aland Islands", "value": "AX" },
				{ "caption": "Albania", "value": "AL" },
				{ "caption": "Algeria", "value": "DZ" },
				{ "caption": "American Samoa", "value": "AS" },
				{ "caption": "Andorra", "value": "AD" },
				{ "caption": "Angola", "value": "AO" },
				{ "caption": "Anguilla", "value": "AI" },
				{ "caption": "Antarctica", "value": "AQ" },
				{ "caption": "Antigua and Barbuda", "value": "AG" },
				{ "caption": "Argentina", "value": "AR" },
				{ "caption": "Armenia", "value": "AM" },
				{ "caption": "Aruba", "value": "AW" },
				{ "caption": "Australia", "value": "AU" },
				{ "caption": "Austria", "value": "AT" },
				{ "caption": "Azerbaijan", "value": "AZ" },
				{ "caption": "Bahamas", "value": "BS" },
				{ "caption": "Bahrain", "value": "BH" },
				{ "caption": "Bangladesh", "value": "BD" },
				{ "caption": "Barbados", "value": "BB" },
				{ "caption": "Belarus", "value": "BY" },
				{ "caption": "Belgium", "value": "BE" },
				{ "caption": "Belize", "value": "BZ" },
				{ "caption": "Benin", "value": "BJ" },
				{ "caption": "Bermuda", "value": "BM" },
				{ "caption": "Bhutan", "value": "BT" },
				{ "caption": "Bolivia, Plurinational State of", "value": "BO" },
				{ "caption": "Bonaire, Sint Eustatius and Saba", "value": "BQ" },
				{ "caption": "Bosnia and Herzegovina", "value": "BA" },
				{ "caption": "Botswana", "value": "BW" },
				{ "caption": "Bouvet Island", "value": "BV" },
				{ "caption": "Brazil", "value": "BR" },
				{ "caption": "British Indian Ocean Territory", "value": "IO" },
				{ "caption": "Brunei Darussalam", "value": "BN" },
				{ "caption": "Bulgaria", "value": "BG" },
				{ "caption": "Burkina Faso", "value": "BF" },
				{ "caption": "Burundi", "value": "BI" },
				{ "caption": "Cambodia", "value": "KH" },
				{ "caption": "Cameroon", "value": "CM" },
				{ "caption": "Canada", "value": "CA" },
				{ "caption": "Cape Verde", "value": "CV" },
				{ "caption": "Cayman Islands", "value": "KY" },
				{ "caption": "Central African Republic", "value": "CF" },
				{ "caption": "Chad", "value": "TD" },
				{ "caption": "Chile", "value": "CL" },
				{ "caption": "China", "value": "CN" },
				{ "caption": "Christmas Island", "value": "CX" },
				{ "caption": "Cocos (Keeling) Islands", "value": "CC" },
				{ "caption": "Colombia", "value": "CO" },
				{ "caption": "Comoros", "value": "KM" },
				{ "caption": "Congo", "value": "CG" },
				{ "caption": "Congo, the Democratic Republic of the", "value": "CD" },
				{ "caption": "Cook Islands", "value": "CK" },
				{ "caption": "Costa Rica", "value": "CR" },
				{ "caption": "Cote d\"Ivoire", "value": "CI" },
				{ "caption": "Croatia", "value": "HR" },
				{ "caption": "Cuba", "value": "CU" },
				{ "caption": "Curacao", "value": "CW" },
				{ "caption": "Cyprus", "value": "CY" },
				{ "caption": "Czech Republic", "value": "CZ" },
				{ "caption": "Denmark", "value": "DK" },
				{ "caption": "Djibouti", "value": "DJ" },
				{ "caption": "Dominica", "value": "DM" },
				{ "caption": "Dominican Republic", "value": "DO" },
				{ "caption": "Ecuador", "value": "EC" },
				{ "caption": "Egypt", "value": "EG" },
				{ "caption": "El Salvador", "value": "SV" },
				{ "caption": "Equatorial Guinea", "value": "GQ" },
				{ "caption": "Eritrea", "value": "ER" },
				{ "caption": "Estonia", "value": "EE" },
				{ "caption": "Ethiopia", "value": "ET" },
				{ "caption": "Falkland Islands (Malvinas)", "value": "FK" },
				{ "caption": "Faroe Islands", "value": "FO" },
				{ "caption": "Fiji", "value": "FJ" },
				{ "caption": "Finland", "value": "FI" },
				{ "caption": "France", "value": "FR" },
				{ "caption": "French Guiana", "value": "GF" },
				{ "caption": "French Polynesia", "value": "PF" },
				{ "caption": "French Southern Territories", "value": "TF" },
				{ "caption": "Gabon", "value": "GA" },
				{ "caption": "Gambia", "value": "GM" },
				{ "caption": "Georgia", "value": "GE" },
				{ "caption": "Germany", "value": "DE" },
				{ "caption": "Ghana", "value": "GH" },
				{ "caption": "Gibraltar", "value": "GI" },
				{ "caption": "Greece", "value": "GR" },
				{ "caption": "Greenland", "value": "GL" },
				{ "caption": "Grenada", "value": "GD" },
				{ "caption": "Guadeloupe", "value": "GP" },
				{ "caption": "Guam", "value": "GU" },
				{ "caption": "Guatemala", "value": "GT" },
				{ "caption": "Guernsey", "value": "GG" },
				{ "caption": "Guinea", "value": "GN" },
				{ "caption": "Guinea-Bissau", "value": "GW" },
				{ "caption": "Guyana", "value": "GY" },
				{ "caption": "Haiti", "value": "HT" },
				{ "caption": "Heard Island and McDonald Islands", "value": "HM" },
				{ "caption": "Holy See (Vatican City State)", "value": "VA" },
				{ "caption": "Honduras", "value": "HN" },
				{ "caption": "Hong Kong", "value": "HK" },
				{ "caption": "Hungary", "value": "HU" },
				{ "caption": "Iceland", "value": "IS" },
				{ "caption": "India", "value": "IN" },
				{ "caption": "Indonesia", "value": "ID" },
				{ "caption": "Iran, Islamic Republic of", "value": "IR" },
				{ "caption": "Iraq", "value": "IQ" },
				{ "caption": "Ireland", "value": "IE" },
				{ "caption": "Isle of Man", "value": "IM" },
				{ "caption": "Israel", "value": "IL" },
				{ "caption": "Italy", "value": "IT" },
				{ "caption": "Jamaica", "value": "JM" },
				{ "caption": "Japan", "value": "JP" },
				{ "caption": "Jersey", "value": "JE" },
				{ "caption": "Jordan", "value": "JO" },
				{ "caption": "Kazakhstan", "value": "KZ" },
				{ "caption": "Kenya", "value": "KE" },
				{ "caption": "Kiribati", "value": "KI" },
				{ "caption": "Korea, Democratic People\"s Republic of", "value": "KP" },
				{ "caption": "Korea, Republic of", "value": "KR" },
				{ "caption": "Kuwait", "value": "KW" },
				{ "caption": "Kyrgyzstan", "value": "KG" },
				{ "caption": "Lao People\"s Democratic Republic", "value": "LA" },
				{ "caption": "Latvia", "value": "LV" },
				{ "caption": "Lebanon", "value": "LB" },
				{ "caption": "Lesotho", "value": "LS" },
				{ "caption": "Liberia", "value": "LR" },
				{ "caption": "Libya", "value": "LY" },
				{ "caption": "Liechtenstein", "value": "LI" },
				{ "caption": "Lithuania", "value": "LT" },
				{ "caption": "Luxembourg", "value": "LU" },
				{ "caption": "Macao", "value": "MO" },
				{ "caption": "Macedonia, the Former Yugoslav Republic of", "value": "MK" },
				{ "caption": "Madagascar", "value": "MG" },
				{ "caption": "Malawi", "value": "MW" },
				{ "caption": "Malaysia", "value": "MY" },
				{ "caption": "Maldives", "value": "MV" },
				{ "caption": "Mali", "value": "ML" },
				{ "caption": "Malta", "value": "MT" },
				{ "caption": "Marshall Islands", "value": "MH" },
				{ "caption": "Martinique", "value": "MQ" },
				{ "caption": "Mauritania", "value": "MR" },
				{ "caption": "Mauritius", "value": "MU" },
				{ "caption": "Mayotte", "value": "YT" },
				{ "caption": "Mexico", "value": "MX" },
				{ "caption": "Micronesia, Federated States of", "value": "FM" },
				{ "caption": "Moldova, Republic of", "value": "MD" },
				{ "caption": "Monaco", "value": "MC" },
				{ "caption": "Mongolia", "value": "MN" },
				{ "caption": "Montenegro", "value": "ME" },
				{ "caption": "Montserrat", "value": "MS" },
				{ "caption": "Morocco", "value": "MA" },
				{ "caption": "Mozambique", "value": "MZ" },
				{ "caption": "Myanmar", "value": "MM" },
				{ "caption": "Namibia", "value": "NA" },
				{ "caption": "Nauru", "value": "NR" },
				{ "caption": "Nepal", "value": "NP" },
				{ "caption": "Netherlands", "value": "NL" },
				{ "caption": "New Caledonia", "value": "NC" },
				{ "caption": "New Zealand", "value": "NZ" },
				{ "caption": "Nicaragua", "value": "NI" },
				{ "caption": "Niger", "value": "NE" },
				{ "caption": "Nigeria", "value": "NG" },
				{ "caption": "Niue", "value": "NU" },
				{ "caption": "Norfolk Island", "value": "NF" },
				{ "caption": "Northern Mariana Islands", "value": "MP" },
				{ "caption": "Norway", "value": "NO" },
				{ "caption": "Oman", "value": "OM" },
				{ "caption": "Pakistan", "value": "PK" },
				{ "caption": "Palau", "value": "PW" },
				{ "caption": "Palestine, State of", "value": "PS" },
				{ "caption": "Panama", "value": "PA" },
				{ "caption": "Papua New Guinea", "value": "PG" },
				{ "caption": "Paraguay", "value": "PY" },
				{ "caption": "Peru", "value": "PE" },
				{ "caption": "Philippines", "value": "PH" },
				{ "caption": "Pitcairn", "value": "PN" },
				{ "caption": "Poland", "value": "PL" },
				{ "caption": "Portugal", "value": "PT" },
				{ "caption": "Puerto Rico", "value": "PR" },
				{ "caption": "Qatar", "value": "QA" },
				{ "caption": "Reunion", "value": "RE" },
				{ "caption": "Romania", "value": "RO" },
				{ "caption": "Russian Federation", "value": "RU" },
				{ "caption": "Rwanda", "value": "RW" },
				{ "caption": "Saint Barthelemy", "value": "BL" },
				{ "caption": "Saint Helena, Ascension and Tristan da Cunha", "value": "SH" },
				{ "caption": "Saint Kitts and Nevis", "value": "KN" },
				{ "caption": "Saint Lucia", "value": "LC" },
				{ "caption": "Saint Martin (French part)", "value": "MF" },
				{ "caption": "Saint Pierre and Miquelon", "value": "PM" },
				{ "caption": "Saint Vincent and the Grenadines", "value": "VC" },
				{ "caption": "Samoa", "value": "WS" },
				{ "caption": "San Marino", "value": "SM" },
				{ "caption": "Sao Tome and Principe", "value": "ST" },
				{ "caption": "Saudi Arabia", "value": "SA" },
				{ "caption": "Senegal", "value": "SN" },
				{ "caption": "Serbia", "value": "RS" },
				{ "caption": "Seychelles", "value": "SC" },
				{ "caption": "Sierra Leone", "value": "SL" },
				{ "caption": "Singapore", "value": "SG" },
				{ "caption": "Sint Maarten (Dutch part)", "value": "SX" },
				{ "caption": "Slovakia", "value": "SK" },
				{ "caption": "Slovenia", "value": "SI" },
				{ "caption": "Solomon Islands", "value": "SB" },
				{ "caption": "Somalia", "value": "SO" },
				{ "caption": "South Africa", "value": "ZA" },
				{ "caption": "South Georgia and the South Sandwich Islands", "value": "GS" },
				{ "caption": "South Sudan", "value": "SS" },
				{ "caption": "Spain", "value": "ES" },
				{ "caption": "Sri Lanka", "value": "LK" },
				{ "caption": "Sudan", "value": "SD" },
				{ "caption": "Suriname", "value": "SR" },
				{ "caption": "Svalbard and Jan Mayen", "value": "SJ" },
				{ "caption": "Swaziland", "value": "SZ" },
				{ "caption": "Sweden", "value": "SE" },
				{ "caption": "Switzerland", "value": "CH" },
				{ "caption": "Syrian Arab Republic", "value": "SY" },
				{ "caption": "Taiwan, Province of China", "value": "TW" },
				{ "caption": "Tajikistan", "value": "TJ" },
				{ "caption": "Tanzania, United Republic of", "value": "TZ" },
				{ "caption": "Thailand", "value": "TH" },
				{ "caption": "Timor-Leste", "value": "TL" },
				{ "caption": "Togo", "value": "TG" },
				{ "caption": "Tokelau", "value": "TK" },
				{ "caption": "Tonga", "value": "TO" },
				{ "caption": "Trinidad and Tobago", "value": "TT" },
				{ "caption": "Tunisia", "value": "TN" },
				{ "caption": "Turkey", "value": "TR" },
				{ "caption": "Turkmenistan", "value": "TM" },
				{ "caption": "Turks and Caicos Islands", "value": "TC" },
				{ "caption": "Tuvalu", "value": "TV" },
				{ "caption": "Uganda", "value": "UG" },
				{ "caption": "Ukraine", "value": "UA" },
				{ "caption": "United Arab Emirates", "value": "AE" },
				{ "caption": "United Kingdom", "value": "GB" },
				{ "caption": "United States", "value": "US" },
				{ "caption": "United States Minor Outlying Islands", "value": "UM" },
				{ "caption": "Uruguay", "value": "UY" },
				{ "caption": "Uzbekistan", "value": "UZ" },
				{ "caption": "Vanuatu", "value": "VU" },
				{ "caption": "Venezuela, Bolivarian Republic of", "value": "VE" },
				{ "caption": "Viet Nam", "value": "VN" },
				{ "caption": "Virgin Islands, British", "value": "VG" },
				{ "caption": "Virgin Islands, U.S.", "value": "VI" },
				{ "caption": "Wallis and Futuna", "value": "WF" },
				{ "caption": "Western Sahara", "value": "EH" },
				{ "caption": "Yemen", "value": "YE" },
				{ "caption": "Zambia", "value": "ZM" },
				{ "caption": "Zimbabwe", "value": "ZW" }
	        ]
	    },
	    "organisation_purchase_auth": {
	    	"type": "dropdown",
	    	"values": [
	    		{ 'caption': 'Authorise', 'value': 'Authorise' },
	    		{ 'caption': 'Recommend', 'value': 'Recommend' },
	    		{ 'caption': 'Specify', 'value': 'Specify' },
	    		{ 'caption': 'None', 'value': 'None' },
	    	]
	    },
	    "organisation_employee_num": {
	    	"type": "dropdown",
	    	"values": [
	    		{ 'caption': '0-19', 'value': '0-19' },
	    		{ 'caption': '20-49', 'value': '20-49' },
	    		{ 'caption': '50-99', 'value': '50-99' },
	    		{ 'caption': '100-499', 'value': '100-499' },
	    		{ 'caption': '500-999', 'value': '500-999' },
	    		{ 'caption': '1000+', 'value': '1000+' }
	    	]
	    },
	    "organisation_type": {
	    	"type": "dropdown",
	    	"values": [
	    		{ "caption": "Industrial/Commercial", "value": "Industrial/Commercial" },
	    		{ "caption": "Academic/educational/Not-for-profit", "value": "Academic/educational/Not-for-profit" },
	    		{ "caption": "Government/Regulatory", "value": "Government/Regulatory" }
	    	]
	    },
	    "accessories" : {"type": "checkbox", "values": [{"value": true}]},
	    "amplifiers_and_power_supplies" : {"type": "checkbox", "values": [{"value": true}]},
	    "analysis_test_and_measurement" : {"type": "checkbox", "values": [{"value": true}]},
	    "bioinformatics" : {"type": "checkbox", "values": [{"value": true}]},
	    "books" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_area_scan" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_ccd" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_cmos" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_line_scan" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_other" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_smart" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_thermal_imaging" : {"type": "checkbox", "values": [{"value": true}]},
	    "cameras_and_imaging" : {"type": "checkbox", "values": [{"value": true}]},
	    "cheminformatics" : {"type": "checkbox", "values": [{"value": true}]},
	    "chromatography_data_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "cleaning" : {"type": "checkbox", "values": [{"value": true}]},
	    "complete_laser_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "complete_vision_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "components_and_subsystems" : {"type": "checkbox", "values": [{"value": true}]},
	    "consulting_services" : {"type": "checkbox", "values": [{"value": true}]},
	    "control_and_guidance" : {"type": "checkbox", "values": [{"value": true}]},
	    "cutting_heavy" : {"type": "checkbox", "values": [{"value": true}]},
	    "cutting_light" : {"type": "checkbox", "values": [{"value": true}]},
	    "databases" : {"type": "checkbox", "values": [{"value": true}]},
	    "displays" : {"type": "checkbox", "values": [{"value": true}]},
	    "drilling" : {"type": "checkbox", "values": [{"value": true}]},
	    "e_learning" : {"type": "checkbox", "values": [{"value": true}]},
	    "electronic_lab_notebooks_elns" : {"type": "checkbox", "values": [{"value": true}]},
	    "electronics" : {"type": "checkbox", "values": [{"value": true}]},
	    "engraving" : {"type": "checkbox", "values": [{"value": true}]},
	    "enterprise_resource_planning_software" : {"type": "checkbox", "values": [{"value": true}]},
	    "fibre_optics" : {"type": "checkbox", "values": [{"value": true}]},
	    "fibres_and_cabling" : {"type": "checkbox", "values": [{"value": true}]},
	    "frame_grabbers" : {"type": "checkbox", "values": [{"value": true}]},
	    "fttx_access_network_equipment" : {"type": "checkbox", "values": [{"value": true}]},
	    "graphics" : {"type": "checkbox", "values": [{"value": true}]},
	    "high_performance_computing" : {"type": "checkbox", "values": [{"value": true}]},
	    "illumination" : {"type": "checkbox", "values": [{"value": true}]},
	    "image_analysis_and_processing" : {"type": "checkbox", "values": [{"value": true}]},
	    "information_management_products" : {"type": "checkbox", "values": [{"value": true}]},
	    "laboratory_data_management" : {"type": "checkbox", "values": [{"value": true}]},
	    "laboratory_information_management_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "laser_additive_manufacturing" : {"type": "checkbox", "values": [{"value": true}]},
	    "laser_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "lasers_and_diodes" : {"type": "checkbox", "values": [{"value": true}]},
	    "lasers" : {"type": "checkbox", "values": [{"value": true}]},
	    "learned_journals_electronic_or_paper" : {"type": "checkbox", "values": [{"value": true}]},
	    "leds_and_illumination" : {"type": "checkbox", "values": [{"value": true}]},
	    "lenses_and_optics" : {"type": "checkbox", "values": [{"value": true}]},
	    "lenses" : {"type": "checkbox", "values": [{"value": true}]},
	    "library_automation_products_or_services" : {"type": "checkbox", "values": [{"value": true}]},
	    "manufacturing_resource_management" : {"type": "checkbox", "values": [{"value": true}]},
	    "marking" : {"type": "checkbox", "values": [{"value": true}]},
	    "mass_spectrometry" : {"type": "checkbox", "values": [{"value": true}]},
	    "materials" : {"type": "checkbox", "values": [{"value": true}]},
	    "mathematics_simulation_and_modelling" : {"type": "checkbox", "values": [{"value": true}]},
	    "micromachining" : {"type": "checkbox", "values": [{"value": true}]},
	    "microscopy" : {"type": "checkbox", "values": [{"value": true}]},
	    "molecular_modelling" : {"type": "checkbox", "values": [{"value": true}]},
	    "optical_transmission_equipment" : {"type": "checkbox", "values": [{"value": true}]},
	    "optics" : {"type": "checkbox", "values": [{"value": true}]},
	    "other_online_services_or_products" : {"type": "checkbox", "values": [{"value": true}]},
	    "other" : {"type": "checkbox", "values": [{"value": true}]},
	    "patent_trademark_or_ipr_information" : {"type": "checkbox", "values": [{"value": true}]},
	    "physics_and_engineering_instruments" : {"type": "checkbox", "values": [{"value": true}]},
	    "positioning_equipment" : {"type": "checkbox", "values": [{"value": true}]},
	    "product_lifecycle_management" : {"type": "checkbox", "values": [{"value": true}]},
	    "records_management_or_archiving" : {"type": "checkbox", "values": [{"value": true}]},
	    "safety" : {"type": "checkbox", "values": [{"value": true}]},
	    "scientific_document_management_systems" : {"type": "checkbox", "values": [{"value": true}]},
	    "sensors_and_detectors" : {"type": "checkbox", "values": [{"value": true}]},
	    "sensors" : {"type": "checkbox", "values": [{"value": true}]},
	    "services_and_software" : {"type": "checkbox", "values": [{"value": true}]},
	    "software" : {"type": "checkbox", "values": [{"value": true}]},
	    "spectroscopy" : {"type": "checkbox", "values": [{"value": true}]},
	    "statistics" : {"type": "checkbox", "values": [{"value": true}]},
	    "subscription_services" : {"type": "checkbox", "values": [{"value": true}]},
	    "switches_and_routers" : {"type": "checkbox", "values": [{"value": true}]},
	    "test_and_measurement" : {"type": "checkbox", "values": [{"value": true}]},
	    "training_and_education" : {"type": "checkbox", "values": [{"value": true}]},
	    "visualisation" : {"type": "checkbox", "values": [{"value": true}]},
	    "web_based_services_and_products" : {"type": "checkbox", "values": [{"value": true}]},
	    "welding_heavy" : {"type": "checkbox", "values": [{"value": true}]},
	    "welding_light" : {"type": "checkbox", "values": [{"value": true}]}
	};

/*ADD DESCRIPTIONS TO THE LABELS OF CHECKBOXES*/
function addDescriptionsToLabels(parentSelector) {
	var elmnt = document.querySelector(parentSelector);
	var labels = elmnt.getElementsByTagName('label');
	
	for(var i = 0; i < labels.length; i++) {
		addDescriptionToLabelEV(labels[i]);
	}
	
	function addDescriptionToLabelEV(label) {
		var newElement = document.createElement("div");
		newElement.className = "description-for-checkbox";/*"description-choose-prod";*/
		var textNode;
		var textNodeString;
		var privacyPolicyOptin = false;
		
		switch(label.getAttribute("for")) {
		/*EO*/
			case "electrooptics_subscription_digital":
				textNodeString = "Receive digital copies of Electro Optics magazine  (suitable for desktop, mobile and tablet), 10 times a year, delivered to your email address on the day of publication. Free worldwide.";
				break;
			case "electrooptics_subscription_print":
				textNodeString = "Receive printed copies of Electro Optics, 10 times a year, delivered to your postal address. Free to European addresses; \u00A350 per year outside Europe.";
				break;
			case "electrooptics_subscription_newsline":
				textNodeString = "Our monthly email newsletter highlighting the most significant photonics news, events, opinion, comment and feature articles from our website.";
				break;
			case "electrooptics_subscription_productline":
				textNodeString = "Our regular product email newsletter (published 1-2 times per month), containing the very latest in photonics technology.";
				break;
			case "electrooptics_subscription_tech_focus":
				textNodeString = "An occasional themed email newsletter on specific areas of photonics technology. We'll only send you one if the topics are relevant to you";
				break;
			case "electrooptics_subscription_wc_wp":
				textNodeString = "Acquire in-depth knowledge about latest photonics technologies through our informative webcasts and white papers. We'll alert you when a relevant webcast or white paper is published.";
				break;
			case "electrooptics_subscription_advertising":
				textNodeString = "Interested in marketing or advertising with Electro Optics? Sign up to hear about upcoming opportunities.";
				break;
			case "electrooptics_subscription_editorial":
				textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and product round-ups";
				break;
		/*FS*/
            case "fibresystems_subscription_digital":
            	textNodeString = "The digital edition of Fibre Systems magazine; Published quarterly, and delivered to your email address on the day of publication";
            	break;
            case "fibresystems_subscription_print":
            	textNodeString = "The print copy of Fibre Systems magazine, published quarterly. Free to European addresses; \u00A320 per year outside Europe";
            	break;
            case "fibresystems_subscription_newsline":
            	textNodeString = "Our popular email newsletter highlighting the most significant news, events, comment and feature articles from our website";
            	break;
            case "fibresystems_subscription_productline":
            	textNodeString = "Our monthly product email newsletter showcasing a selection of products from carefully selected suppliers";
            	break;
            case "fibresystems_subscription_wc_wp":
            	textNodeString = "Our regular email newsletter bringing you in-depth information from our library of white papers and vendor articles ";
            	break;
            case "fibresystems_subscription_advertising":
            	textNodeString = "Interested in marketing or advertising with Fibre Systems? Sign up to hear about upcoming opportunities.";
            	break;
            case "fibresystems_subscription_editorial":
            	textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and product round-ups";
            	break;
		/*IMVE*/
			case "imagingandmachinevision_subscription_digital":
				textNodeString = "Receive digital copies of Imaging and Machine Vision Europe magazine (suitable for desktop, mobile and tablet), six times a year plus our annual Vision Yearbook, delivered to your email address on the day of publication. Free worldwide.";
				break;
			case "imagingandmachinevision_subscription_print":
				textNodeString = "Receive printed copies of Imaging and Machine Vision Europe, six times a year plus our annual Vision Yearbook, delivered to your postal address. Free to European addresses; \u00A330 per year outside Europe.";
				break;
			case "imagingandmachinevision_subscription_newsline":
				textNodeString = "Our monthly email newsletter highlighting the most significant machine vision news, events, opinion, comment and feature articles from our website.";
				break;
			case "imagingandmachinevision_subscription_productline":
				textNodeString = "Our regular product email newsletter (published 1-2 times per month), containing the very latest in machine vision technology.";
				break;
			case "imagingandmachinevision_subscription_tech_focus":
				textNodeString = "An occasional themed email newsletter on specific areas of imaging technology. We'll only send you one if the topics are relevant to you.";
				break;
			case "imagingandmachinevision_subscription_wc_wp":
				textNodeString = "Acquire in-depth knowledge about latest machine vision technologies through our informative webcasts and white papers. We'll alert you when a relevant webcast or white paper is published.";
				break;
			case "imagingandmachinevision_subscription_advertising":
				textNodeString = "Interested in marketing or advertising with Imaging and Machine Vision Europe? Sign up to hear about upcoming opportunities.";
				break;
			case "imagingandmachinevision_subscription_editorial":
				textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and product round-ups.";
				break;
		/*LSE*/
			case "lasersystemseurope_subscription_digital":
				textNodeString = "Receive digital copies of Laser Systems Europe magazine (suitable for desktop, mobile and tablet), four times a year, delivered to your email address on the day of publication. Free worldwide.";
				break;
			case "lasersystemseurope_subscription_print":
				textNodeString = "Receive printed copies of Laser Systems Europe, four times a year, delivered to your postal address. Free to European addresses; \u00A320 per year outside Europe.";
				break;
			case "lasersystemseurope_subscription_newsline":
				textNodeString = "Our monthly email newsletter highlighting the most significant industrial laser news, events, opinion, comment and feature articles from our website.";
				break;
			case "lasersystemseurope_subscription_productline":
				textNodeString = "Our regular product email newsletter (published 1-2 times per month), containing the very latest in industrial laser technology.";
				break;
			case "lasersystemseurope_subscription_tech_focus":
				textNodeString = "An occasional themed email newsletter on specific areas of industrial laser technology. We'll only send you one if the topics are relevant to you.";
				break;
			case "lasersystemseurope_subscription_wc_wp":
				textNodeString = "Acquire in-depth knowledge about latest industrial laser technologies through our informative webcasts and white papers. We'll alert you when a relevant webcast or white paper is published.";
				break;
			case "lasersystemseurope_subscription_advertising":
				textNodeString = "Interested in marketing or advertising with Laser Systems Europe? Sign up to hear about upcoming opportunities.";
				break;
			case "lasersystemseurope_subscription_editorial":
				textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and product round-ups.";
				break;
		/*RI*/
			case "researchinformation_subscription_digital":
				textNodeString = "Receive digital copies of Research Information magazine  (suitable for desktop, mobile and tablet), six times a year, delivered to your email address on the day of publication. Free worldwide.";
				break;
			case "researchinformation_subscription_print":
				textNodeString = "Receive printed copies of Research Information, six times a year, delivered to your postal address. Free to European addresses.";
				break;
			case "researchinformation_subscription_newsline":
				textNodeString = "Our monthly email newsletter highlighting the most significant scholarly communications news, events, opinion, comment and feature articles from our website.";
				break;
			case "researchinformation_subscription_wc_wp":
				textNodeString = "Acquire in-depth knowledge about scholarly communications through our informative webcasts and white papers. We'll alert you when a relevant webcast or white paper is published.";
				break;
			case "researchinformation_subscription_advertising":
				textNodeString = "Interested in marketing or advertising with Research Information? Sign up to hear about upcoming opportunities.";
				break;
			case "researchinformation_subscription_editorial":
				textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and our Analysis and Opinion section";
				break;
		/*SCW*/
			case "scientificcomputingworld_subscription_digital":
				textNodeString = "Receive digital copies of Scientific Computing World magazine  (suitable for desktop, mobile and tablet), 10 times a year, delivered to your email address on the day of publication. Free worldwide";
				break;
			case "scientificcomputingworld_subscription_print":
				textNodeString = "Receive printed copies of Scientific Computing World, 10 times a year, delivered to your postal address. Free to European addresses; \u00A350 per year outside Europe.";
				break;
			case "scientificcomputingworld_subscription_newsline":
				textNodeString = "Our monthly email newsletter highlighting the most significant photonics news, events, opinion, comment and feature articles from our website.";
				break;
			case "scientificcomputingworld_subscription_hpc_newsline":
				textNodeString = "Our monthly email newsletter dedicated to HPC content highlighting the most significant news, events, opinion, comment and feature articles from our website.";
				break;
			case "scientificcomputingworld_subscription_informatics":
				textNodeString = "Our monthly email newsletter dedicated to informatics content highlighting the most significant news, events, opinion, comment and feature articles from our website.";
				break;
			case "scientificcomputingworld_subscription_productline":
				textNodeString = "Our regular product email newsletter (published 1-2 times per month), containing the very latest in computing technology.";
				break;
			case "scientificcomputingworld_subscription_mod_sim_newsline":
				textNodeString = "Our monthly email newsletter dedicated to modelling and simulation content highlighting the most significant news, events, opinion, comment and feature articles from our website.";
				break;
			case "scientificcomputingworld_subscription_wc_wp":
				textNodeString = "Acquire in-depth knowledge about latest computing technologies through our informative webcasts and white papers. We'll alert you when a relevant webcast or white paper is published.";
				break;
			case "scientificcomputingworld_subscription_advertising":
				textNodeString = "Interested in marketing or advertising with Scientific Computing World? Sign up to hear about upcoming opportunities.";
				break;
			case "scientificcomputingworld_subscription_editorial":
				textNodeString = "Advance notice by email of what's on the editorial calendar, giving you a chance to contribute to upcoming articles and product round-ups";
				break;
		/*EUROPA-WIDE OPT-INS*/
			case "eurosci_optin":
				textNodeString = "Acquire in-depth knowledge about latest photonics technologies through our informative webcasts and white papers (published 1-2 times per month)";
				break;
			case "third_party_advertising_flag":
				textNodeString = "Occasionally, we would like to send you tailored information on behalf of selected, relevant, third parties, including special offers on products, conferences, events, and so on. Your data will never be shared with these third parties by us. To ensure you don't miss out on these special offers, please check this box.";
				break;
		/*MAG SPECIFIC OPT-INS*/
			case "eo_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Electro Optics and accept the ";
				privacyPolicyOptin = true;
				break;
			case "fs_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Fibre Systems and accept the ";
				privacyPolicyOptin = true;
				break;
			case "imve_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Imaging and Machine Vision Europe and accept the ";
				privacyPolicyOptin = true;
				break;
			case "lse_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Laser Systems Europe and accept the ";
				privacyPolicyOptin = true;
				break;
			case "ri_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Research Information and accept the ";
				privacyPolicyOptin = true;
				break;
			case "scw_optin":
				textNodeString = "Yes, I'd like to sign up to receive information from Scientific Computing World and accept the ";
				privacyPolicyOptin = true;
				break;
			case "eo_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Electro Optics partners (any information will come from Electro Optics, and your email address will not be passed on to any third party).";
				break;
			case "fs_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Fibre Systems partners (any information will come from Fibre Systems, and your email address will not be passed on to any third party).";
				break;
			case "imve_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Imaging and Machine Vision Europe partners (any information will come from Imaging and Machine Vision Europe, and your email address will not be passed on to any third party).";
				break;
			case "lse_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Laser Systems Europe partners (any information will come from Laser Systems Europe, and your email address will not be passed on to any third party).";
				break;
			case "ri_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Research Information partners (any information will come from Research Information, and your email address will not be passed on to any third party).";
				break;
			case "scw_third_party":
				textNodeString = "Yes, I'd like to receive selected special offers and information from Scientific Computing World partners (any information will come from Scientific Computing World, and your email address will not be passed on to any third party).";
				break;
			case "ri_optin_report_18":			
				textNodeString = "Yes, I would like to receive the Challenges in Scholarly Publishing Cycle 2018 report via email.";
				break;
			case "ri_optin_report_18_dig_sci":
				textNodeString = "Yes, I would like to hear from Digital Science about its events, and tools for researchers, institutions, publishers and funders";
				break;
			case "ri_optin_report_18_highwire":
				textNodeString = "Yes, I would like to learn more about HighWire’s platform capability and its suite of applications that support pre-prints, manuscript submissions, identity management, analytics and e-commerce.";
				break;
			default:
				break;
		}
		function insertAfter(newNode, referenceNode) {
			referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
		}
		// need to add in the link to the privacy policy if its a required opt in
		if (privacyPolicyOptin) {
			if (textNodeString) {
				var a = document.createElement('a');
				var linkText = document.createTextNode("privacy policy");
				a.appendChild(linkText);
				a.title = "view the privacy policy";
				a.href = "http://www.europascience.com/privacy_policy.html";
				a.target = "_blank";
				textNode = document.createTextNode(textNodeString);
				newElement.appendChild(textNode);
				newElement.appendChild(a);
				insertAfter(newElement, label);
				return;
			}
		}
		else {
			if (textNodeString) {
				textNode = document.createTextNode(textNodeString);
				newElement.appendChild(textNode);
				insertAfter(newElement, label);
			}
		}
	}
}

function fixLabel(parentSelector, name) {
    var label = document.querySelector(parentSelector).querySelector('label[for="' + name + '"]');
    if (!label) {
    	return;
    }
    var daddy = label.parentNode;
    var newLabel = document.createElement("div");
    newLabel.className = "col-sm-9";
    label.className = label.className.replace("col-sm-3", "");
    newLabel.appendChild(label);
    el = daddy.querySelectorAll(".col-sm-9");
    var i;
    for (i = 0; i < el.length; i++) {
        if (el[i].children.length == 0) el[i].parentNode.removeChild(el[i]);
        else el[i].className = el[i].className.replace("col-sm-9", "col-sm-3");
    }
    daddy.appendChild(newLabel);
    var clearfix = document.createElement("div");

    clearfix.className = "clearfix";
    daddy.appendChild(clearfix);

}

EV.Event.on("mainWidgets", function()  {
	function areCheckboxesReady() {
	    var expectedCheckboxes = parseInt(2) + parseInt(Drupal.settings.europa.regHiddenAttrCheckBox.split(",").length);
	    var currentCheckboxes = document.querySelectorAll('#evolok-register input[type="checkbox"]').length;
	    return expectedCheckboxes == currentCheckboxes;
	}
	
	function bindEventsToWidgetLoaded() {
		var parentSelector = "#evolok-register";
		fixLabel(parentSelector, "eo_optin");
		fixLabel(parentSelector, "fs_optin");
		fixLabel(parentSelector, "imve_optin");
		fixLabel(parentSelector, "lse_optin");
		fixLabel(parentSelector, "ri_optin");
		fixLabel(parentSelector, "scw_optin");
		fixLabel(parentSelector, "eo_third_party");
		fixLabel(parentSelector, "fs_third_party");
		fixLabel(parentSelector, "imve_third_party");
		fixLabel(parentSelector, "lse_third_party");
		fixLabel(parentSelector, "ri_third_party");
		fixLabel(parentSelector, "scw_third_party");
		addDescriptionsToLabels("#evolok-register");
	}

	var areCheckboxesReadyInterval = setInterval(function () {
	    if (areCheckboxesReady()) {
	    	bindEventsToWidgetLoaded();
	        clearInterval(areCheckboxesReadyInterval);
	    }
	}, 500);
});

/*	listens using a bespoke value (reg-page-widget-loaded) which is set
 * by myself in the /register page content like this:
 * 
 * <ev-registration recaptcha="true" ev-ready="reg-page-widget-loaded" 	*/
EV.Event.on("reg-page-widget-loaded", function()  {

	function areCheckboxesReady() {
	    var expectedCheckboxes = 2;
	    var currentCheckboxes = document.querySelectorAll('#evolok-register input[type="checkbox"]').length;
	    return expectedCheckboxes == currentCheckboxes;
	}

	function bindEventsToWidgetLoaded() {
		var parentSelector = "#evolok-register";
		fixLabel(parentSelector, "eo_optin");
		fixLabel(parentSelector, "fs_optin");
		fixLabel(parentSelector, "imve_optin");
		fixLabel(parentSelector, "lse_optin");
		fixLabel(parentSelector, "ri_optin");
		fixLabel(parentSelector, "scw_optin");
		fixLabel(parentSelector, "eo_third_party");
		fixLabel(parentSelector, "fs_third_party");
		fixLabel(parentSelector, "imve_third_party");
		fixLabel(parentSelector, "lse_third_party");
		fixLabel(parentSelector, "ri_third_party");
		fixLabel(parentSelector, "scw_third_party");
		addDescriptionsToLabels("#evolok-register");
	}

	var areCheckboxesReadyInterval = setInterval(function () {
	    if (areCheckboxesReady()) {
	    	bindEventsToWidgetLoaded();
	        clearInterval(areCheckboxesReadyInterval);
	    }
	}, 500);
});

//var regModalOptinLabelsFixed = false;
var isFirstCall = true;
/*	listens using a bespoke value which is set
 * by myself in the /subscription page content like this:
 * 
 * <ev-registration recaptcha="true" ev-ready="reg-page-widget-loaded" 	*/
EV.Event.on("incomplete-reg-prompt", function()  {
	
	if (isFirstCall) {
		isFirstCall = false;
		return;
	}
	
	function areCheckboxesReady() {
	    var expectedCheckboxes = 1;
	    var currentCheckboxes = document.querySelectorAll('.modal-content label[for*="_optin"]').length;
	    return expectedCheckboxes == currentCheckboxes;
	}

	function bindEventsToWidgetLoaded() {
		var parentSelector = ".modal-content";
		//layout is slightly different for the modal, so probably shouldn't fix it 
		//fixLabel(parentSelector, "eo_optin");
		//fixLabel(parentSelector, "fs_optin");
		//fixLabel(parentSelector, "imve_optin");
		//fixLabel(parentSelector, "lse_optin");
		//fixLabel(parentSelector, "ri_optin");
		//fixLabel(parentSelector, "scw_optin");
		//if (!regModalOptinLabelsFixed) {
			addDescriptionsToLabels(".modal-content");
		//	regModalOptinLabelsFixed = true;
		//}
	}

	var areCheckboxesReadyInterval = setInterval(function () {
	    if (areCheckboxesReady()) {
	    	bindEventsToWidgetLoaded();
	        clearInterval(areCheckboxesReadyInterval);
	    }
	}, 500);
});



/**
 * 	manage required opt-in per site
 * 
 */

var optInModalDisplayed = false;
showModalIfNoOptin(EV.util.getEVSession());
EV.Event.on(EV.Event.SESSION_REFRESH, function (session) {
    showModalIfNoOptin(session);
});

function showModalIfNoOptin(session) {
    if (!session || optInModalDisplayed) return;
    var lowerCaseMagInitials = Drupal.settings.europa.magShortname.toLowerCase();
    var requiredServiceName = lowerCaseMagInitials + "_required_optin";
    var requiredAttrName = lowerCaseMagInitials + "_optin";
    EV.Core.getUserProfile(session.guid, requiredServiceName, true).then(function (res) {
        var found = res.response.userProfile.attributes.find(function (attr) { return attr.name == requiredAttrName && attr.value == "true"; });
        if (!found) {
        	var buttons = document.querySelectorAll(".europa-evolok-inner-wrap #edit-wrap button.ev.btn.btn-success");
        	var i;
        	for (i = 0; i < buttons.length; i++) {
        		//buttons[i].disabled = true;
        		//document.querySelector(".europa-evolok-inner-wrap #edit-wrap").style.display = "none";
        	}
            var optInModalDisplayed = true;
            setTimeout(function() {
            		EV.Event.publish("ev.open.modal.evolok-required-optin");
            	}, 1000);
            //Drupal.settings.europa.chooseReqOptinService 
        }
    });
}


/**
 * 		After successful registration, redirect
 * 		users to the welcome page
 */
EV.Event.on("registration.success", function (res) {
	// Get saved data from sessionStorage
	var data = sessionStorage.getItem("blockRegRedirect");
	
	if (data != "true") { 	
		//Redirect them to the welcome page
		window.open("/evolok/welcome", "_self");
	}
});



var sesh = EV.util.getEVSession();
if ((window.location.pathname == "/register" || window.location.pathname == "/evolok/register") && sesh) {
	EV.Core.getUserProfile(sesh.guid, requiredServiceName, true).then(function (res) {
		var found = res.response.userProfile.attributes.find(function (attr) { return attr.name == requiredAttrName && attr.value == "true"; });
		if (!found) {
			window.location.replace(window.location.protocol+"//"+window.location.hostname+"/subscription");
		}
	});
}


/**
 * 		If the user is logged in and visits "/register" 
 * 		redirect them to "/subscription"
 */
var sesh = EV.util.getEVSession();
if ((window.location.pathname == "/register" || window.location.pathname == "/evolok/register") && sesh) {
    window.location.replace(window.location.protocol+"//"+window.location.hostname+"/subscription");
}

/**
 * 		Fix the country label
 */
function countryLabelFix() {
	var isCountryReadyInterval = setInterval(function () {
	    if (document.querySelectorAll("label[for=\"country\"]")) {
	    	//currently have 3 so needs updating if add more
			var divs = document.querySelectorAll("label[for=\"country\"]");
			var i;
			var c = 0;
			for (i = 0; i < divs.length; ++i) {
			//don't want to do this forever, so once we've got 3 instances stop
			  if (divs[i].innerHTML != "Country *") {
				  divs[i].innerHTML = "Country *";
				  c++;
			  }
			}
			if (c >= 3) {
				clearInterval(isCountryReadyInterval);
			}
	    }
	}, 500);
}
countryLabelFix();

/**
 * 		Guide the user down to where the forgot password widget 
 * 		will appear (works around the issue of hiding the 
 * 		flip-back stuff in the europa.css)
 */
var isForgotPassReadyInterval = setInterval(function () {
	function checkForgotPassReady() {
		return document.querySelector(".forgot-password");
	}
    if (checkForgotPassReady()) {
    	var forgotpass = document.querySelector(".forgot-password");
    	forgotpass.addEventListener("click", function(){
    	    window.scrollBy(0,250);
    	});
        clearInterval(isForgotPassReadyInterval);
    }
}, 500);


/**
 * 		Handle 2/3 page of subscription form unchecking of 
 * 		'receive information from us' by unchecking the 
 * 		other options that people may currently have ticked
 * 		and doing nothing if they cancel during the 'confirm'
 */
var timer = setInterval(function() {
	var el = document.querySelector("#evolok-choose-products label[for*=\"_optin\"]")
	if (el) {
		var myString = el.innerHTML;
		if (myString.charAt(myString.length - 1) == '*') {
			myString = myString.substr(0, myString.length - 1);
			el.innerHTML = myString;
		}
		var checkbox = el.parentNode.querySelector("input[type=\"checkbox\"]")
		checkbox.addEventListener("change", function(){
			if(!this.checked) {
				if(confirm("By unchecking this box you will no longer receive any information from "+Drupal.settings.europa.magFullName+" , we will now untick any remaining selections. Are you sure you want to continue?")) {
					//	console.log("no longer receive");
					var els = document.querySelectorAll("#evolok-choose-products input[type=\"checkbox\"]")
					for (var i = 0; i < els.length; i++) {
						/*below excludes the third party from being unchecked along with the other products
						if(!els[i].parentNode.parentNode.parentNode.parentNode.querySelector("label[for*=\"_third_party\"")) {
							els[i].checked = false;
						}*/
						els[i].checked = false;
					}
				}
				else {
					//	console.log("continue to receive");
				}
			}
		});
		clearInterval(timer);
	}
}, 100);

//ri report fix labels
document.getElementById("access-ri-report-18").addEventListener("click", function() {
	EV.Event.publish('ev.open.modal.access-modal');
});

EV.Event.on("register-ri-report-18", function() {
	setTimeout(fix, 1000);
});