/*
 *	@file
 *
 *		config script for the Evolok Access Director
 * 
 **/
EV.Dab.init();//init the adblock bait with the Evolok library
EV.Em.init({
    url: 'https://eur.evolok.net/acd/api/3.0',
	sidCookieDomain: "." + window.location.hostname.replace("www.", "")
});
// set in the Drupal DOM object through the europa.module file + additional info like adblock being detected 
var key_and_vals = {articleId:Drupal.settings.europa.articleId, Referer:window.location.href, section:Drupal.settings.europa.section, tags:Drupal.settings.europa.tags, author:Drupal.settings.europa.authors, site:Drupal.settings.europa.site, supertags:Drupal.settings.europa.supertag, category:Drupal.settings.europa.category, adblock:EV.Dab.isAdblockDetected()};
params = JSON.stringify(key_and_vals);
EV.Em.authorize(params, function(response, params) {
	/*success callback*/
	var resultState = response ? response.result : null;
    if (resultState == "ALLOW_ACCESS") {
    	//access granted
    }
    else {
    	/*jQuery("body").addClass("ev"); "dirty workaround" but only if we're using modals (and only temporarily (Aug 2017) */
    	var res = document.getElementsByClassName("node-"+Drupal.settings.europa.articleId);
    	var node = res[0];
		node.innerHTML='Please <a href="/subscription">register or login</a> to view this content.'
    	//node.style.display = "none";
    	console.log("content hidden");
    }
}, function() {
	/*error callback*/
});

function hideContent(articleId) {
	console.log("articleId="+articleId);
}