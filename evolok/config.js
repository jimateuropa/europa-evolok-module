EV.Widgets = {Display: {}};
EV.Core.init({
	serviceName: "registration",					// the default service name
	realmName: "default_realm",						// the default realm name
	icDomain: "https://eur.evolok.net/ic/api",	// the Identity Core endpoint to use
	recaptchaSiteKey: Drupal.settings.europa.recaptchaKey,
		//	this is the public key for electrooptics's config
		//	we set this in the admin/config/system/europa page
		//	and add it in the europa_page_build() function
		//	to the Drupal.settings.europa.
	//secureMode: true,//secureMode: true,								// if enabled, then set ex_ss secure cookie
	//ssoCookieDomain: 		// set the ev_ss or ex_ss (secureMore = true) cookie for subdomain suffix
});
EV.Social.init({
    icDomain: "https://eur.evolok.net/ic/api",
    serviceName: "social",
    realmName: "default_realm",
    redirectUri: "/evolok/subscription",
    socialKeys: {
        facebook  : Drupal.settings.europa.facebookKey, //"674873639370909",
        google    : Drupal.settings.europa.googleKey, // "886524228982-klmjsf4lig3f8nee22jn103p3gc95q1m.apps.googleusercontent.com",
        twitter   : Drupal.settings.europa.twitterKey, // "spfHPHPwYQGxY0SD0lIZw6PqP",
        linkedin  : Drupal.settings.europa.linkedinKey, // "78ei37ogbkdx81",
    },
	socialAttributeMapping: {
		/* first = IC attribute : second = social profile attribute */
	    "email_address" : "email",
	    "first_name" : "firstName",
	    "last_name" : "lastName"
	    //list from Carlos
	    /*userId, firstName, lastName, screenname, email, gender, birthday, network, location, industry, positions, skills, interests, ageRange
	    	there is no list by network, but you can guess
	    	and in the socialSettings configuration you can see the fields each network has
	    	oh, screenname** */
	}
});
EV.Event.publish("ev.open.modal.evolok-register");

//customise buttons & error messages
EV.Translate.addTranslationTable("eur", {
	ev: {
		widgets: {
			/*attr: {
				email_address: {
					label: "Email",
					placeholder: "",
				},
				password: {
					label: "Password *",
					placeholder: "",
					confirm: {
						label: "Confirm Password *",
						placeholder: "",
					}
				},
				_password: {
					label: "Password *",
					placeholder: "",
					confirm: {
						label: "Confirm Password *",
						placeholder: "",
					}
				},
				password_: {
					label: "Password *",
					placeholder: "",
					confirm: {
						label: "Confirm Password *",
						placeholder: "",
					}
				},
				first_name: {
					placeholder: "",
				},
				last_name: {
					placeholder: "",
				},
			},*/
		errors: {
				duplicate_user_profile: "This email address is already in use. You can log in/ request a new password by selecting the log in tab. If you created your account on a different Europa Science site, you may need to reactivate it here.",
				//identifiers_not_found: "Sorry, 123123123, there is no account associated with this email address",
				identifiers_invalid: "The email or password you entered is incorrect",
				//require_unique_profile_and_realm: "This email address is already in use, please try another",
				//user_profile_identifiers_not_found: "Hello world",
				forgot_password_success: "You will receive an email with a link that will allow you to change your password. If you have not received an email within a few minutes, please check your email spam folder and that you have entered your email correctly. If this does not resolve your issue, please email subscriptions@europascience.com.",
				//attribute_validation_failed: "Please enter a valid email address",
				//edit_successful: "Your details have been updated",
				//validators_value_empty: "Please enter a password",
				//wrong_credentials: "There are many mistakes {0}"
				general_attributes : "There is an error with the following fields: <em>{0}</em>",
				general_attribute : "There is an error with the field: <em>{0}</em>",
				attribute_required: "It is required that you check 'Receive information from " + Drupal.settings.europa.magFullName + "' to create a valid account.",
				/*	DOESN'T WORK WITH CHECKBOXES.... (can presumably do it with others?)
				 * eo_optin: "It is required that you check 'Receive information from Electro Optics' to create a valid account.",
				fs_optin: "It is required that you check 'Receive information from Fibre Systems' to create a valid account.",
				imve_optin: "It is required that you check 'Receive information from Imaging and Machine Vision Europe' to create a valid account.",
				lse_optin: "It is required that you check 'Receive information from Laser Systems Europe' to create a valid account.",
				ri_optin: "It is required that you check 'Receive information from Research Information' to create a valid account.",
				scw_optin: "It is required that you check 'Receive information from Scientific Computing World' to create a valid account."*/
			}
		}
	}
});
EV.Translate.setActiveLanguage("eur");