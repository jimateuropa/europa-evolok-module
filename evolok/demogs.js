switch(Drupal.settings.europa.site) {
	case "EO":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
			    { "caption": "Engineer or scientist working in R&D", "value": "Engineer or scientist working in R&D" },
			    { "caption": "Engineer or scientist working in Design", "value": "Engineer or scientist working in Design" },
			    { "caption": "Engineer or scientist working in Manufacturing or Production", "value": "Engineer or scientist working in Manufacturing or Production" },
			    { "caption": "Engineer or scientist working in Testing, quality control, or safety", "value": "Engineer or scientist working in Testing, quality control, or safety" },
			    { "caption": "Engineer or scientist working in Other", "value": "Engineer or scientist working in Other" },
			    { "caption": "Manager, senior manager, or owner responsible for Corporate", "value": "Manager, senior manager, or owner responsible for Corporate" },
				{ "caption": "Manager, senior manager, or owner responsible for Engineering/Technical", "value": "Manager, senior manager, or owner responsible for Engineering/Technical" },
				{ "caption": "Manager, senior manager, or owner responsible for Manufacturing or Production", "value": "Manager, senior manager, or owner responsible for Manufacturing or Production" },
				{ "caption": "Manager, senior manager, or owner responsible for Other", "value": "Manager, senior manager, or owner responsible for Other" },
				{ "caption": "Manager, senior manager, or owner responsible for Purchasing/Finance", "value": "Manager, senior manager, or owner responsible for Purchasing/Finance" },
				{ "caption": "Professor/Lecturer or other Educator", "value": "Professor/Lecturer or other Educator" },
				{ "caption": "Consultant", "value": "Consultant" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Aerospace/aviation systems", "value": "Aerospace/aviation systems" },
				{ "caption": "Agriculture/Food", "value": "Agriculture/Food" },
				{ "caption": "Analytical, test, or measurement instrumentation", "value": "Analytical, test, or measurement instrumentation" },
				{ "caption": "Automotive", "value": "Automotive" },
				{ "caption": "Chemical or pharmaceutical products", "value": "Chemical or pharmaceutical products" },
				{ "caption": "Communications equipment", "value": "Communications equipment" },
				{ "caption": "Consultancy", "value": "Consultancy" },
				{ "caption": "Consumer electronics/Other consumer products", "value": "Consumer electronics/Other consumer products" },
				{ "caption": "Detectors/sensors/cameras", "value": "Detectors/sensors/cameras" },
				{ "caption": "Displays (electronic etc)", "value": "Displays (electronic etc)" },
				{ "caption": "Education/training", "value": "Education/training" },
				{ "caption": "Electronics/semiconductors/optoelectronics", "value": "Electronics/semiconductors/optoelectronics" },
				{ "caption": "Energy/Renewables", "value": "Energy/Renewables" },
				{ "caption": "Engineering", "value": "Engineering" },
				{ "caption": "Environmental monitoring", "value": "Environmental monitoring" },
				{ "caption": "Glass", "value": "Glass" },
				{ "caption": "Government/Regulatory", "value": "Government/Regulatory" },
				{ "caption": "Image processing/Machine vision", "value": "Image processing/Machine vision" },
				{ "caption": "Industrial/manufacturing equipment", "value": "Industrial/manufacturing equipment" },
				{ "caption": "Lasers/laser systems", "value": "Lasers/laser systems" },
				{ "caption": "LEDs/Other lighting products", "value": "LEDs/Other lighting products" },
				{ "caption": "Medical and biotechnology equipment or products", "value": "Medical and biotechnology equipment or products" },
				{ "caption": "Military/Defence/Security/Law enforcement", "value": "Military/Defence/Security/Law enforcement" },
				{ "caption": "Navigation/Guidance systems", "value": "Navigation/Guidance systems" },
				{ "caption": "Optical components/materials/systems", "value": "Optical components/materials/systems" },
				{ "caption": "Packaging", "value": "Packaging" },
				{ "caption": "Paper", "value": "Paper" },
				{ "caption": "Photographic/printing/publishing", "value": "Photographic/printing/publishing" },
				{ "caption": "Research", "value": "Research" },
				{ "caption": "Robotics/process control", "value": "Robotics/process control" },
				{ "caption": "Timber/forestry", "value": "Timber/forestry" },
				{ "caption": "Transport", "value": "Transport" },
				{ "caption": "Other (please state)", "value": "Other (please state)" },
			]
		};
		break;
	case "FS":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
			    { "caption": "Corporate management or company owner", "value": "Corporate management or company owner" },
			    { "caption": "Consultant", "value": "Consultant" },
			    { "caption": "Engineer or scientist working in R&D", "value": "Engineer or scientist working in R&D" },
			    { "caption": "Engineer or scientist working in Design", "value": "Engineer or scientist working in Design" },
			    { "caption": "Engineer or scientist working in software development", "value": "Engineer or scientist working in software development" },
			    { "caption": "Engineer or scientist working in Manufacturing or Production", "value": "Engineer or scientist working in Manufacturing or Production" },
			    { "caption": "Engineer or scientist working in Testing, quality control, or safety", "value": "Engineer or scientist working in Testing, quality control, or safety" },
			    { "caption": "Engineer or scientist working in Other", "value": "Engineer or scientist working in Other" },
			    { "caption": "Manager, senior manager, or owner responsible for Corporate", "value": "Manager, senior manager, or owner responsible for Corporate" },
				{ "caption": "Manager, senior manager, or owner responsible for Engineering/Technical", "value": "Manager, senior manager, or owner responsible for Engineering/Technical" },
				{ "caption": "Manager, senior manager, or owner responsible for Manufacturing or Production", "value": "Manager, senior manager, or owner responsible for Manufacturing or Production" },
				{ "caption": "Manager, senior manager, or owner responsible for Purchasing/Finance", "value": "Manager, senior manager, or owner responsible for Purchasing/Finance" },
				{ "caption": "Professor/Lecturer or other Educator", "value": "Professor/Lecturer or other Educator" },
				{ "caption": "Public relations and corporate communication", "value": "Public relations and corporate communication" },
				{ "caption": "Technician responsible for installation and maintenance", "value": "Technician responsible for installation and maintenance" },
				{ "caption": "Student", "value": "Student" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Access broadband systems", "value": "Access broadband systems" },
				{ "caption": "Alternative national telco", "value": "Alternative national telco" },
				{ "caption": "Broadband access provider", "value": "Broadband access provider" },
				{ "caption": "Business/enterprise fibre optics", "value": "Business/enterprise fibre optics" },
				{ "caption": "Carriers' carrier", "value": "Carriers’ carrier" },
				{ "caption": "Component and subsystems automation", "value": "Component and subsystems automation" },
				{ "caption": "Consultancy", "value": "Consultancy" },
				{ "caption": "Data centre/network storage", "value": "Data centre/network storage" },
				{ "caption": "Distributor/reseller", "value": "Distributor/reseller" },
				{ "caption": "Education/institution", "value": "Education/institution" },
				{ "caption": "Embedded software/development", "value": "Embedded software/development" },
				{ "caption": "Fibre optic installation", "value": "Fibre optic installation" },
				{ "caption": "Financial services", "value": "Financial services" },
				{ "caption": "General network systems", "value": "General network systems" },
				{ "caption": "Government/military", "value": "Government/military" },
				{ "caption": "Incumbent national telco", "value": "Incumbent national telco" },
				{ "caption": "International long-haul telco", "value": "International long-haul telco" },
				{ "caption": "Internet service provider", "value": "Internet service provider" },
				{ "caption": "Long/ultra long haul systems", "value": "Long/ultra long haul systems" },
				{ "caption": "Medical/healthcare", "value": "Medical/healthcare" },
				{ "caption": "Metro systems", "value": "Metro systems" },
				{ "caption": "Metro/ regional telco", "value": "Metro/ regional telco" },
				{ "caption": "Military/aerospace equipment", "value": "Military/aerospace equipment" },
				{ "caption": "Other (please specify)", "value": "Other (please specify)" },
				{ "caption": "Regional systems", "value": "Regional systems" },
				{ "caption": "Research and development", "value": "Research and development" },
				{ "caption": "Test and measurement equipment", "value": "Test and measurement equipment" },
				{ "caption": "Utility/energy company", "value": "Utility/energy company" },
			]
		};
		break;
	case "IMVE":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
			    { "caption": "Engineer or scientist working in R&D", "value": "Engineer or scientist working in R&D" },
			    { "caption": "Engineer or scientist working in Design", "value": "Engineer or scientist working in Design" },
			    { "caption": "Engineer or scientist working in Manufacturing or Production", "value": "Engineer or scientist working in Manufacturing or Production" },
			    { "caption": "Engineer or scientist working in Testing, quality control, or safety", "value": "Engineer or scientist working in Testing, quality control, or safety" },
			    { "caption": "Engineer or scientist working in Other", "value": "Engineer or scientist working in Other" },
			    { "caption": "Manager, senior manager, or owner responsible for Corporate", "value": "Manager, senior manager, or owner responsible for Corporate" },
				{ "caption": "Manager, senior manager, or owner responsible for Engineering/Technical", "value": "Manager, senior manager, or owner responsible for Engineering/Technical" },
				{ "caption": "Manager, senior manager, or owner responsible for Manufacturing or Production", "value": "Manager, senior manager, or owner responsible for Manufacturing or Production" },
				{ "caption": "Manager, senior manager, or owner responsible for Other", "value": "Manager, senior manager, or owner responsible for Other" },
				{ "caption": "Manager, senior manager, or owner responsible for Purchasing/Finance", "value": "Manager, senior manager, or owner responsible for Purchasing/Finance" },
				{ "caption": "Professor/Lecturer or other Educator", "value": "Professor/Lecturer or other Educator" },
				{ "caption": "Consultant", "value": "Consultant" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Aerospace/aviation systems", "value": "Aerospace/aviation systems" },
				{ "caption": "Agriculture/Food", "value": "Agriculture/Food" },
				{ "caption": "Analytical, test, or measurement instrumentation", "value": "Analytical, test, or measurement instrumentation" },
				{ "caption": "Automotive", "value": "Automotive" },
				{ "caption": "Chemical or pharmaceutical products", "value": "Chemical or pharmaceutical products" },
				{ "caption": "Communications equipment", "value": "Communications equipment" },
				{ "caption": "Consultancy", "value": "Consultancy" },
				{ "caption": "Consumer electronics/Other consumer products", "value": "Consumer electronics/Other consumer products" },
				{ "caption": "Detectors/sensors/cameras", "value": "Detectors/sensors/cameras" },
				{ "caption": "Displays (electronic etc)", "value": "Displays (electronic etc)" },
				{ "caption": "Education/training", "value": "Education/training" },
				{ "caption": "Electronics/semiconductors/optoelectronics", "value": "Electronics/semiconductors/optoelectronics" },
				{ "caption": "Energy/Renewables", "value": "Energy/Renewables" },
				{ "caption": "Engineering", "value": "Engineering" },
				{ "caption": "Environmental monitoring", "value": "Environmental monitoring" },
				{ "caption": "Glass", "value": "Glass" },
				{ "caption": "Government/Regulatory", "value": "Government/Regulatory" },
				{ "caption": "Image processing/Machine vision", "value": "Image processing/Machine vision" },
				{ "caption": "Industrial/manufacturing equipment", "value": "Industrial/manufacturing equipment" },
				{ "caption": "Lasers/laser systems", "value": "Lasers/laser systems" },
				{ "caption": "LEDs/Other lighting products", "value": "LEDs/Other lighting products" },
				{ "caption": "Medical and biotechnology equipment or products", "value": "Medical and biotechnology equipment or products" },
				{ "caption": "Military/Defence/Security/Law enforcement", "value": "Military/Defence/Security/Law enforcement" },
				{ "caption": "Navigation/Guidance systems", "value": "Navigation/Guidance systems" },
				{ "caption": "Optical components/materials/systems", "value": "Optical components/materials/systems" },
				{ "caption": "Packaging", "value": "Packaging" },
				{ "caption": "Paper", "value": "Paper" },
				{ "caption": "Photographic/printing/publishing", "value": "Photographic/printing/publishing" },
				{ "caption": "Research", "value": "Research" },
				{ "caption": "Robotics/process control", "value": "Robotics/process control" },
				{ "caption": "Timber/forestry", "value": "Timber/forestry" },
				{ "caption": "Transport", "value": "Transport" },
				{ "caption": "Other (please state)", "value": "Other (please state)" },
			]
		};	
		break;
	case "LSE":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
			    { "caption": "Engineer or scientist working in R&D", "value": "Engineer or scientist working in R&D" },
			    { "caption": "Engineer or scientist working in Design", "value": "Engineer or scientist working in Design" },
			    { "caption": "Engineer or scientist working in Manufacturing or Production", "value": "Engineer or scientist working in Manufacturing or Production" },
			    { "caption": "Engineer or scientist working in Testing, quality control, or safety", "value": "Engineer or scientist working in Testing, quality control, or safety" },
			    { "caption": "Engineer or scientist working in Other", "value": "Engineer or scientist working in Other" },
			    { "caption": "Manager, senior manager, or owner responsible for Corporate", "value": "Manager, senior manager, or owner responsible for Corporate" },
				{ "caption": "Manager, senior manager, or owner responsible for Engineering/Technical", "value": "Manager, senior manager, or owner responsible for Engineering/Technical" },
				{ "caption": "Manager, senior manager, or owner responsible for Manufacturing or Production", "value": "Manager, senior manager, or owner responsible for Manufacturing or Production" },
				{ "caption": "Manager, senior manager, or owner responsible for Other", "value": "Manager, senior manager, or owner responsible for Other" },
				{ "caption": "Manager, senior manager, or owner responsible for Purchasing/Finance", "value": "Manager, senior manager, or owner responsible for Purchasing/Finance" },
				{ "caption": "Professor/Lecturer or other Educator", "value": "Professor/Lecturer or other Educator" },
				{ "caption": "Consultant", "value": "Consultant" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Aerospace/aviation systems", "value": "Aerospace/aviation systems" },
				{ "caption": "Automotive", "value": "Automotive" },
				{ "caption": "Consultancy", "value": "Consultancy" },
				{ "caption": "Consumer electronics/Other consumer products", "value": "Consumer electronics/Other consumer products" },
				{ "caption": "Displays (electronic etc)", "value": "Displays (electronic etc)" },
				{ "caption": "Education/training", "value": "Education/training" },
				{ "caption": "Electronics/semiconductors/optoelectronics", "value": "Electronics/semiconductors/optoelectronics" },
				{ "caption": "Engineering", "value": "Engineering" },
				{ "caption": "Glass", "value": "Glass" },
				{ "caption": "Government/Regulatory", "value": "Government/Regulatory" },
				{ "caption": "Industrial/manufacturing equipment", "value": "Industrial/manufacturing equipment" },
				{ "caption": "Job shop", "value": "Job shop" },
				{ "caption": "Medical and biotechnology equipment or products", "value": "Medical and biotechnology equipment or products" },
				{ "caption": "Metal fabrication", "value": "Metal fabrication" },
				{ "caption": "Military/Defence/Security/Law enforcement", "value": "Military/Defence/Security/Law enforcement" },
				{ "caption": "OEM/Systems integration", "value": "OEM/Systems integration" },
				{ "caption": "Research", "value": "Research" },
				{ "caption": "Textiles", "value": "Textiles" },
				{ "caption": "Timber/forestry", "value": "Timber/forestry" },
				{ "caption": "Transport", "value": "Transport" },
				{ "caption": "Other (please state)", "value": "Other (please state)" },
			]
		};
		break;
	case "RI":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Consultant", "value": "Consultant" },
				{ "caption": "Database/IT Professional", "value": "Database IT Professional" },
				{ "caption": "Educator", "value": "Educator" },
				{ "caption": "Information Manager", "value": "Information Manager" },
				{ "caption": "Information Scientist", "value": "Information Scientist" },
				{ "caption": "Lecturer/Professor", "value": "Lecturer_Professor" },
				{ "caption": "Librarian/Information Professional", "value": "Librarian Information Professional" },
				{ "caption": "Library Manager", "value": "Library Manager" },
				{ "caption": "Publishing", "value": "Publishing" },
				{ "caption": "Scientist/Scientific Researcher", "value": "Scientist Scientific Researcher" },
				{ "caption": "Subscription Agent", "value": "Subscription Agent" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
			"type": "dropdown",
			"values": [
				{ "caption": "Bio/Pharma/Medical", "value": "Bio/Pharma/Medical" },
				{ "caption": "Maths/Stats", "value": "Maths/Stats" },
				{ "caption": "Physics", "value": "Physics" },
				{ "caption": "Chemistry", "value": "Chemistry" },
				{ "caption": "Engineering", "value": "Engineering" },
				{ "caption": "Environment", "value": "Environment" },
				{ "caption": "IT/Computing", "value": "IT/Computing" },
				{ "caption": "Social sciences", "value": "Social sciences" },
				{ "caption": "Humanities", "value": "Humanities" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		break;
	case "SCW":
		EV.Widgets.Display["job_role"] = {
			"type": "dropdown",
			"values": [
			    { "caption": "Engineer or scientist working in R&D", "value": "Engineer or scientist working in R&D" },
			    { "caption": "Engineer or scientist working in Design", "value": "Engineer or scientist working in Design" },
			    { "caption": "Engineer or scientist working in Manufacturing or Production", "value": "Engineer or scientist working in Manufacturing or Production" },
			    { "caption": "Engineer or scientist working in Testing, quality control, or safety", "value": "Engineer or scientist working in Testing, quality control, or safety" },
			    { "caption": "Engineer or scientist working in Other", "value": "Engineer or scientist working in Other" },
			    { "caption": "Manager, senior manager, or owner responsible for Corporate", "value": "Manager, senior manager, or owner responsible for Corporate" },
				{ "caption": "Manager, senior manager, or owner responsible for Engineering/Technical", "value": "Manager, senior manager, or owner responsible for Engineering/Technical" },
				{ "caption": "Manager, senior manager, or owner responsible for Manufacturing or Production", "value": "Manager, senior manager, or owner responsible for Manufacturing or Production" },
				{ "caption": "Manager, senior manager, or owner responsible for Other", "value": "Manager, senior manager, or owner responsible for Other" },
				{ "caption": "Manager, senior manager, or owner responsible for Purchasing/Finance", "value": "Manager, senior manager, or owner responsible for Purchasing/Finance" },
				{ "caption": "Professor/Lecturer or other Educator", "value": "Professor/Lecturer or other Educator" },
				{ "caption": "Consultant", "value": "Consultant" },
				{ "caption": "Other", "value": "Other" },
			]
		};
		EV.Widgets.Display["organisation_scope"] = {
				"type": "dropdown",
				"values": [
					{ "caption": "Aerospace/aviation systems", "value": "Aerospace/aviation systems" },
					{ "caption": "Agricultural/Food/Beverage", "value": "Agricultural/Food/Beverage" },
					{ "caption": "Analytical, test or measurement instrumentation", "value": "Analytical, test or measurement instrumentation" },
					{ "caption": "Automotive", "value": "Automotive" },
					{ "caption": "Chemical (but not petrochemicals or pharmaceuticals)", "value": "Chemical (but not petrochemicals or pharmaceuticals)" },
					{ "caption": "Computer and IT Services", "value": "Computer and IT Services" },
					{ "caption": "Computer Hardware", "value": "Computer Hardware" },
					{ "caption": "Computer Software", "value": "Computer Software" },
					{ "caption": "Consultancy", "value": "Consultancy" },
					{ "caption": "Education/training/research", "value": "Education/training/research" },
					{ "caption": "Electronics/semiconductors", "value": "Electronics/semiconductors" },
					{ "caption": "Energy/renewables (but not oil/petrochemicals)", "value": "Energy/renewables (but not oil/petrochemicals)" },
					{ "caption": "Engineering", "value": "Engineering" },
					{ "caption": "Environmental monitoring", "value": "Environmental monitoring" },
					{ "caption": "Financial services/economics", "value": "Financial services/economics" },
					{ "caption": "Government/Regulatory", "value": "Government/Regulatory" },
					{ "caption": "Imaging/Machine Vision", "value": "Imaging/Machine Vision" },
					{ "caption": "Industrial/manufacturing equipment", "value": "Industrial/manufacturing equipment" },
					{ "caption": "Medical and biotechnology equipment or products", "value": "Medical and biotechnology equipment or products" },
					{ "caption": "Military/Defence/Security/Law enforcement", "value": "Military/Defence/Security/Law enforcement" },
					{ "caption": "Oil/gas/petrochemicals", "value": "Oil/gas/petrochemicals" },
					{ "caption": "Other (please state)", "value": "Other (please state)" },
					{ "caption": "Pharmaceuticals", "value": "Pharmaceuticals" },
					{ "caption": "Photonics", "value": "Photonics" },
					{ "caption": "Timber/forestry/paper", "value": "Timber/forestry/paper" },
					{ "caption": "Transport", "value": "Transport" },
				]
			};
		break;
	default:
		break;
}