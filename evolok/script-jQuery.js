(function ($, Drupal, window, document, undefined) {
	/*Equiv of document.ready*/
	$(function () {
		if ($("#do-not-contact-form").length) {
			function getCookie(cname) {
			    var name = cname + "=";
			    var decodedCookie = decodeURIComponent(document.cookie);
			    var ca = decodedCookie.split(';');
			    for(var i = 0; i <ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0) == ' ') {
			            c = c.substring(1);
			        }
			        if (c.indexOf(name) == 0) {
			            return c.substring(name.length, c.length);
			        }
			    }
			    return "";
			}
			
			var obj = JSON.parse(EV.util.Storage.session.get("ev_profile"));
			var userGuid = null;
			var objKeys = Object.keys(obj);
			for (var i = 0; i < objKeys.length; ++i) { // Looping is optional. All services should have a "guid" field.
			    if (obj[objKeys[i]].guid) {
			        userGuid = obj[objKeys[i]].guid;
			        break;
			    }
			}
			var ev_ss_val = getCookie("ev_ss");
			$("#do-not-contact-guid").val(userGuid);
			$("#do-not-contact-ss-cookie-val").val(ev_ss_val);
		}
		EV.Event.on("edit-profile-with-lots-of-questions-ready", function()  {
			/*CARLOS' dirty hack 20/07/2017*/
			function isEditProfileWithLotsOfCheckboxesReady() {
			    var expectedCheckboxes = Drupal.settings.europa.demogsCheckboxCount;
			    var currentCheckboxes = $("#evolok-demogs input:checkbox").length;
			    return expectedCheckboxes == currentCheckboxes;
			}

			function bindEventsToEditProfileWithLotsOfCheckboxes() {
			    // your jquery things here
				/*relayout the labels to be after the checkboxes and ideally next to it */
				$("#evolok-demogs input:checkbox").each(function() {
					/*the label that we want to put AFTER the checkbox*/
					var detached_label = $(this).parent().parent().parent().parent().children("label").detach();
					$(this).parent().after(detached_label);
				});
				/*EVOLOK wrap the checkbox demographics for styling*/
				$("#evolok-demogs input:checkbox").parent().parent().parent().parent().parent().wrapAll("<div id='checkbox-demog-wrap' />");
				$("#checkbox-demog-wrap").prepend("<div id=\"checkbox-demog-label\">Which of the following products/services do you purchase/specify (tick all that apply)?</div>");
			}

			var areCheckboxesReadyInterval = setInterval(function () {
			    if (isEditProfileWithLotsOfCheckboxesReady()) {
			        bindEventsToEditProfileWithLotsOfCheckboxes();
			        clearInterval(areCheckboxesReadyInterval);
			    }
			}, 500);
		});
		EV.Event.on("mainWidgets", function()  {
			/*EVOLOK tabbed nav stuff*/
			$("ev-profile ev-profile-when nav ul li").click(function() {
				if (!$(this).hasClass("current-nav-evolok")) {
					$(this).siblings().each(function() {
						if ($(this).hasClass("current-nav-evolok")) {
							$(this).removeClass("current-nav-evolok");
							$(this).addClass("inactive-nav-evolok");
							var classes = $(this).attr('class').split(' ');
							for(var i=0; i<classes.length; i++) {
								if (classes[i].match("evolok-nav-for-")) {
									var res = classes[i].split("for-");
									$("#"+res[1]+"-wrap").hide();
									$("#"+res[1]+"-wrap").removeClass("current-wrap-evolok");
									$("#"+res[1]+"-wrap").addClass("inactive-wrap-evolok");
								}
							}
						}
					});
					$(this).removeClass("inactive-nav-evolok");
					var classes = $(this).attr('class').split(' ');
					for(var i=0; i<classes.length; i++) {
						if (classes[i].match("evolok-nav-for-")) {
							var res = classes[i].split("for-");
							$("#"+res[1]+"-wrap").show();
							$("#"+res[1]+"-wrap").removeClass("inactive-wrap-evolok");
							$("#"+res[1]+"-wrap").addClass("current-wrap-evolok");
						}
					}
					$(this).addClass("current-nav-evolok");
					$(this).removeClass("inactive-nav-evolok");
				}
			});
		});
	});
})(jQuery, Drupal, this, this.document);