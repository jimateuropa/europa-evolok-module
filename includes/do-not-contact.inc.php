<?php

function process_do_not_contact($guid, $ss_cookie_val) {
	$error = FALSE;
	//	Get user profile from Evolok
	$ch = curl_init('https://eur.evolok.net/ic/api/userProfile/' . $guid);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Accept: application/json',
		'Authorization: Evolok evolok.api.service=customer_care evolok.api.sessionId=' . $ss_cookie_val
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$content = json_decode(curl_exec($ch));
	curl_close($ch);
	
	//	Remove all the data from the key / values and leave whitespace to workaround "required" fields
	$attributes = $content->userProfile->attributes;
	$to_encode['attributes'] = array();
	$i = 0;
	$email = '';//may need for logging
	foreach ($attributes as $obj) {
		if ($obj->name != 'email_address') {
			if ($obj->value != ' ') {//need to test update works so we don't want to
					//be including attributes that already have white space in them
					//as whitespace is what lets us test
				$to_encode['attributes'][$i] = new stdClass();
				$to_encode['attributes'][$i]->name = $obj->name;
				$to_encode['attributes'][$i]->value = ' ';
				$i++;
			}
			else {
				//skip it--doesn't need 'deleting'
			}
		}
		else {
			$email = $obj->value;//may need for logging
		}
	}
	
	// 	Send the data back to Evolok
	$ch = curl_init('https://eur.evolok.net/ic/api/userProfile/' . $guid);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	$payload = json_encode($to_encode);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Accept: application/json',
		'Authorization: Evolok evolok.api.service=customer_care evolok.api.sessionId=' . $ss_cookie_val
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);
	
	//	Check Evolok accepted the updates successfully
	$json = json_decode($response);
	$changed_count = count($json->changedAttributes);
	$attempted_count = count($to_encode['attributes']);
	if ($changed_count != $attempted_count) {
		drupal_mail('europa', 'do_not_contact_evolok_error', get_data_manager_email(), language_default(),
				$params = array('email' => $email, 'evolok_response_vals' => json_encode($json->changedAttributes), 
				'evolok_update_vals' => json_encode($to_encode->attributes)), $from = NULL, $send = TRUE);
		watchdog('europa', 'Removing data during deactivation failed, Evolok. @evolok_update_vals | @evolok_response_vals', 
				array('@evolok_update_vals' => json_encode($to_encode['attributes']), 
				'@evolok_response_vals' => json_encode($json->changedAttributes)));
		drupal_set_message(get_do_not_contact_error_msg());
		$error = TRUE;
	}
	
	// 	Send the do not contact info to Evolok
	$ch = curl_init('https://eur.evolok.net/ic/api/userProfile/' . $guid);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	unset($to_encode);
	$to_encode['attributes'] = array();
	$to_encode['attributes'][0] = new stdClass();
	$to_encode['attributes'][0]->name = 'do_not_contact';
	$to_encode['attributes'][0]->value = 'true';
	$payload = json_encode($to_encode);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Accept: application/json',
		'Authorization: Evolok evolok.api.service=do_not_contact evolok.api.sessionId=' . $ss_cookie_val
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);
	
	//	Get user profile from Evolok
	$ch = curl_init('https://eur.evolok.net/ic/api/userProfile/' . $guid);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Accept: application/json',
	'Authorization: Evolok evolok.api.service=do_not_contact evolok.api.sessionId=' . $ss_cookie_val
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$content = json_decode(curl_exec($ch));
	curl_close($ch);
	
	//	Check do not contact is true
	$attributes = $content->userProfile->attributes;
	$i = 0;
	$fail = TRUE;//default to TRUE--force FALSE to be a met condition
	foreach ($attributes as $obj) {
		if ($obj->name == 'do_not_contact') {
			if ($obj->value == 'true') {
				$fail = FALSE;
			}
		}
	}
	
	//	If any error updating do not contact (though potentially it was already 'true')
	if ($fail) {
		drupal_mail('europa', 'do_not_contact_evolok_error_2', get_data_manager_email(), language_default(),
				$params = array('email' => $email, $from = NULL, $send = TRUE));
		watchdog('europa', 'Setting do_not_contact to true failed in Evolok. @email', array('@email' => $email));
		drupal_set_message(get_do_not_contact_error_msg());
		$error = TRUE;
	}

	//	Add email to suppression list on Adestra
	if (module_exists('adestra')) {
		$res = adestra_call('unsubList.addAddress', array($email, 1));//unsub list id = 1
		if ($res != 1) {//from xmlrpc server
			drupal_mail('europa', 'do_not_contact_adestra_update_error', get_data_manager_email(), language_default(),
				$params = array('email' => $email),	$from = NULL, $send = TRUE);
			watchdog('europa', 'Adestra call failed for email @email.', array('@email' => $email));
			drupal_set_message(get_do_not_contact_error_msg());
			$error = TRUE;
		}
	}
	else {
		drupal_mail('europa', 'do_not_contact_adestra_error', get_data_manager_email(), language_default(),
			$params = array('email' => $email),	$from = NULL, $send = TRUE);
		watchdog('europa', 'Adestra module not installed and/or enabled, this is required.');
		drupal_set_message(get_do_not_contact_error_msg());
		$error = TRUE;
	}

	if (!$error) {
		drupal_set_message('We have received your deactivation request and we have removed your information, with
				the exception of your email which we retain on a supression list, to prevent further contact.
				If you would like to exercise your right to be forgotten altogether, contact us directly at 
				<a href="mailto:subscriptions@europascience.com">subscriptions@europascience.com</a>
				or by telephone/post as listed here: <a href="https://www.europascience.com/contact-us/">Contact us</a>.
				However, please note that should we later acquire your email address as a prospective subscriber 
				(under \'legitimate interest\'), you may receive subsequent invitations to re-subscribe, as we 
				would no longer have you on a suppression list.');
		drupal_goto('<front>');
	}
}

function get_do_not_contact_error_msg() {
	return 'There has been an issue processing your request. 
			We apologise for the inconvenience. Please contact us directly at 
			<a href="mailto:subscriptions@europascience.com">subscriptions@europascience.com</a>
			or by telephone/post as listed here: <a href="https://www.europascience.com/contact-us/">Contact us</a>.';
}