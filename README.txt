Europa
======

This is Europa's 2.0 Drupal module approach.
It will work across all sites and feature most prominently the Evolok integration.

Follow the hooks and comments in the code to see how it all works

##Config##

	You can configure module: /admin/config/system/europa

##Evolok integration##

* JavaScript files for integration: https://dl.evolok.net/dist/
* Download it into the module to allow for aggregation when preprocessing is on (drupal_add_js() 'external' disallows this)
* Docs for integration: https://doc.evolok.net/console/index.html#/doc/index
* We don't need to include/use the scripts that are commented-out in europa_page_build()


Author
======

Jim Grantham <jim.grantham@europascience.com>